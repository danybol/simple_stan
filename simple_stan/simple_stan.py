# -*- coding: utf-8 -*-
"""
Created on Wed Feb 25 16:21:55 2015

@author: danybol
"""

import pystan
import pickle
from hashlib import md5
import os
import logging
import platform
import pandas as pd

def stan_cache(model_code, model_name=None, no_run=False, **kwargs):
    """Use just as you would `stan`"""
    code_hash = md5(model_code.encode('ascii')).hexdigest()
    if model_name is None:
        cache_fn = 'cached-model-{}.pkl'.format(code_hash)
        cache_full = os.path.join(os.path.expanduser('~/.stan_cache'),cache_fn)
    else:
        cache_fn = 'cached-{}-{}.pkl'.format(model_name, code_hash)
        cache_full = os.path.join(os.path.expanduser('~/.stan_cache'),cache_fn)
    try:
        sm = pickle.load(open(cache_full, 'rb'))
    except:
        if platform.system()=='Darwin':
            os.environ['CC'] = 'clang'
            os.environ['CXX'] = 'clang'
        sm = pystan.StanModel(model_code=model_code)
        with open(cache_full, 'wb') as f:
            pickle.dump(sm, f)
    else:
        logging.getLogger(__name__).info("Using cached StanModel")
    if no_run:
        return sm
    else:
        return sm.sampling(**kwargs)
        
def load_samples(basename,iter=2000,start=5,chains=4):
    N = iter/2+start
    samples = pd.concat([pd.read_csv("%s_%d.csv" % (basename, i),header=4).iloc[N:,] for i in range(chains)])
    return samples
      
def run_stan_file(model_file,no_run=False, **kwargs):
    #curdir = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.expanduser(model_file),'r') as f:
        model_code = f.read()
    
    return stan_cache(model_code,no_run=no_run,**kwargs)
    
    